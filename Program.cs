﻿using System;
using System.Linq;
using Lu.Crm.Logging;
using Lu.Crm.Time;
using Topshelf;

namespace crm_projectrequestimporter
{
    class Program
    {
        public static void Main(string[] args)
        {
            var consoleMode = (args.Length > 0 && args.Contains("-console"));
            var debugMode = (args.Length > 0 && args.Contains("-debug"));

            WindowsEventLogger.Instance.Initialize("ProjectRequestsImporter", consoleMode, debugMode);

            try
            {
                var topshelfExitCode = HostFactory.Run(hostConfigurator =>
                {
                    hostConfigurator.SetDescription("Imports Banner Project Requests to CRM Dynamics Campaigns.");
                    hostConfigurator.SetDisplayName("ProjectRequestsImporter");
                    hostConfigurator.SetServiceName("ProjectRequestsImporter");
                    hostConfigurator.StartAutomatically();
                    hostConfigurator.Service<TimeMeasurer>
                    (
                        serviceConfigurator =>
                        {
                            serviceConfigurator.ConstructUsing(name => new TimeMeasurer(WindowsEventLogger.Instance));
                            serviceConfigurator.WhenStarted(service => service.Start());
                            serviceConfigurator.BeforeStartingService(hostControl => hostControl.RequestAdditionalTime(TimeSpan.FromSeconds(30)));
                            serviceConfigurator.WhenStopped(service => service.Stop());
                        }
                    );
                    hostConfigurator.AddCommandLineDefinition("console", c => System.Console.WriteLine("Running in console mode."));
                    hostConfigurator.AddCommandLineDefinition("debug", c => System.Console.WriteLine("Running in debug mode."));
                });

                Environment.ExitCode = (int) Convert.ChangeType(topshelfExitCode, topshelfExitCode.GetTypeCode());;
            }
            catch(Exception exception)
            {
                WindowsEventLogger.Instance.Log($"exception message: {exception.Message}\nstacktrace: {exception.StackTrace}");
            }
        }
    }
}
