using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging.EventLog;

namespace Lu.Crm.Logging
{
    public class WindowsEventLogger : ILogger
    {
        private static object logLock = new object();
        private static WindowsEventLogger logger;
        public static WindowsEventLogger Instance => GetInstance();            
        private bool isInitialized;
        private bool consoleMode;
        private bool debugMode;

        private WindowsEventLog eventLog;

        public void Initialize(string logName, bool consoleMode, bool debugMode)
        {
            eventLog = new WindowsEventLog(logName, Environment.MachineName, logName);
            logger.consoleMode = consoleMode;
            logger.debugMode = debugMode;
            isInitialized = true;
        }

        public void Log(string message, EventLogEntryType severity = EventLogEntryType.Information)
        {
            if (!isInitialized) { throw new InvalidOperationException("Logger was not initialized."); }

            eventLog.WriteEntry(message, severity, 0, 0);

            if(consoleMode)
            {
                System.Console.WriteLine(message);
            }
        }

        public void Log(Exception e)
        {
            if (!isInitialized) { throw new InvalidOperationException("Logger was not initialized."); }

            var message = $"{e.Message}\n\r{e.StackTrace}";

            eventLog.WriteEntry(message, EventLogEntryType.Error, 0, 0);

            if(consoleMode)
            {
                System.Console.WriteLine(message);
            }
        }

        private static WindowsEventLogger GetInstance()
        {
            if (logger == null)
            {
                lock (logLock)
                {
                    if (logger == null)
                    {
                        logger = new WindowsEventLogger();
                    }
                }
            }
            return logger;
        }

        public void Debug(string message)
        {
            if (debugMode)
            {
                Log(message);
            }
        }
    }
}