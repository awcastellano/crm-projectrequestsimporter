using System;
using Lu.Crm.Logging;

namespace Lu.Crm.Time
{
    public class TimeMeasurer
    {
        private ILogger _logger;

        public DateTime StartTime{ get; private set; }
        public DateTime EndTime { get; private set; }

        public bool IsRunning
        {
            get
            {
                return StartTime != default(DateTime) && EndTime == default(DateTime);
            }
        }

        public bool HasBeenStarted
        {
            get
            {
                return StartTime != default(DateTime);
            }
        }

        public bool HasBeenStopped
        {
            get
            {
                return EndTime != default(DateTime);
            }
        }

        public TimeMeasurer(ILogger logger)
        {
            _logger = logger;
            logger.Debug("TimeMeasurer constructed");
        }

        public void Start()
        {
            if (StartTime != default(DateTime))
            {
                throw new InvalidOperationException();
            }
            
            StartTime = DateTime.Now;

            _logger.Debug("Timer started.");
        }
        
        public void Stop()
        {
            if (!IsRunning)
            {
                throw new InvalidOperationException();
            }
            
            EndTime = DateTime.Now;

            _logger.Debug("Timer stopped.");
        }
    }
}