using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Lu.Crm.Logging
{
    public interface ILogger
    {
        void Log(string message, EventLogEntryType severity = EventLogEntryType.Information);
        void Log(Exception exception);
        void Debug(string message);
    }
}